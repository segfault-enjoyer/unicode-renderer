const Canvas = @This();

const std = @import("std");
const vecmath = @import("vecmath.zig");

allocator: std.mem.Allocator,
csize: [2]usize,
pixels: [2]usize,
canvas: []u21,

pub fn init(allocator: std.mem.Allocator, csize: [2]usize, psize: [2]usize) !Canvas {
    if (psize[0] == 0 or psize[1] == 0) {
        std.debug.print(
            \\Failed to obtain pixelsize of the terminal. For correct proportions, please use
            \\something like alacritty.
            \\
        , .{});
        return Canvas{
            .allocator = allocator,
            .csize = csize,
            .pixels = .{ csize[0] * 10, csize[1] * 20 },
            .canvas = try allocator.alloc(u21, csize[0] * csize[1]),
        };
    }
    return Canvas{
        .allocator = allocator,
        .csize = csize,
        .pixels = psize,
        .canvas = try allocator.alloc(u21, csize[0] * csize[1]),
    };
}

pub fn deinit(this: *Canvas) void {
    defer this.allocator.free(this.canvas);
}

pub fn print(this: Canvas, allocator: std.mem.Allocator) ![]const u8 {
    var ret = try std.ArrayList(u8).initCapacity(allocator, this.canvas.len * 4);
    errdefer ret.deinit();
    var buf: [4]u8 = undefined;
    for (this.canvas) |cp|
        try ret.appendSlice(buf[0..try std.unicode.utf8Encode(cp, &buf)]);
    return ret.toOwnedSlice();
}

pub fn draw(this: Canvas, a: [2]usize, cp: u21) !void {
    if (a[0] >= this.csize[0] or a[1] >= this.csize[1])
        return error.OutOfBounds;
    this.canvas[vec_to_idx(this.csize, a)] = cp;
}

pub fn fill(this: Canvas, a: [2]usize, b: [2]usize, cp: u21) !void {
    if (b[0] >= this.csize[0] or b[1] >= this.csize[1])
        return error.OutOfBounds;

    for (a[1]..b[1] + 1) |y| {
        for (a[0]..b[0] + 1) |x| {
            this.canvas[vec_to_idx(this.csize, .{ x, y })] = cp;
        }
    }
}

fn vec_to_idx(size: [2]usize, vec: [2]usize) usize {
    return vec[0] + vec[1] * size[0];
}
