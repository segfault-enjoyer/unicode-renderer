const std = @import("std");
const vecmath = @import("vecmath.zig");
const rendering = @import("rendering.zig");
const Canvas = @import("Canvas.zig");
const tty = @import("tty.zig");

pub fn main() !void {
    var alloc_impl = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = alloc_impl.deinit();
    const allocator = alloc_impl.allocator();

    try demo(allocator);

    @import("profile.zig").results();
}

fn demo(allocator: std.mem.Allocator) !void {
    const tty_size = try tty.get_tty_size();
    var canvas = try Canvas.init(
        allocator,
        .{ tty_size.ws_col, tty_size.ws_row },
        .{ tty_size.ws_xpixel, tty_size.ws_ypixel },
    );
    defer canvas.deinit();

    try tty.set_raw();
    defer tty.set_cooked();

    var verticies = std.ArrayList([]const [3]f64).init(allocator);
    defer verticies.deinit();

    var buf: [64 * 1024]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buf);
    const arena = fba.allocator();

    var cam_pos = [3]f64{ 250, -250, -1000 };
    var i: f64 = -std.math.pi;
    main: while (true) : (i += std.math.pi / 64.0) {
        const start_time = std.time.microTimestamp();

        const in = try tty.read(arena);
        for (in) |c| {
            switch (c) {
                'w' => cam_pos[2] += 20,
                's' => cam_pos[2] -= 20,
                'a' => cam_pos[0] -= 20,
                'd' => cam_pos[0] += 20,
                ' ' => cam_pos[1] -= 20,
                127 => cam_pos[1] += 20,
                else => break :main,
            }
        }

        const radius = 300;

        const triangle = try arena.alloc([3]f64, 3);
        triangle[0] = .{ -radius, 0, 0 };
        triangle[1] = .{ radius, 0, 0 };
        triangle[2] = .{ 0, radius, 0 };
        try verticies.append(triangle);

        var j: f64 = -std.math.pi;
        while (j < std.math.pi) : (j += std.math.pi / 64.0) {
            const v0 = try arena.alloc([3]f64, 1);
            const v1 = try arena.alloc([3]f64, 1);
            const v2 = try arena.alloc([3]f64, 1);
            const v3 = try arena.alloc([3]f64, 1);
            v0[0] = .{ 0, 0, j * radius };
            v1[0] = .{ 0, j * radius, 0 };
            v2[0] = .{ j * radius, 0, 0 };
            v3[0] = .{
                std.math.cos(j) * std.math.sin(i) * radius,
                std.math.sin(j) * radius,
                std.math.cos(j) * std.math.cos(i) * radius,
            };
            try verticies.appendSlice(&.{ v0, v1, v2, v3 });
        }
        defer verticies.clearRetainingCapacity();

        try canvas.fill(.{ 0, 0 }, .{ canvas.csize[0] - 1, canvas.csize[1] - 1 }, ' ');
        try rendering.draw(verticies.items, canvas, cam_pos, .{ 0, 0, 1 });

        const out = try canvas.print(arena);
        try std.io.getStdOut().writeAll(out);

        fba.reset();
        try @import("profile.zig").record(@src(), std.time.microTimestamp() - start_time);
        std.time.sleep(std.time.ns_per_s / 64);
    }
}
