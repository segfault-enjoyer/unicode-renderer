//!
//! for each fn that should be measured
//! ```zig
//! const time = std.time.microTimestamp();
//! defer @import("profile.zig").record(@src(), std.time.microTimestamp() - time);
//! ```
//! should be placed on top
//!
//! @import("profile.zig").results();
//! should be run at the very end of the program
//!

const std = @import("std");

const us = i64;
const Measurements = struct {
    src: std.builtin.SourceLocation,
    sum_us: us,
    n_measurements: usize,
};

// uses an arena for minimal runtime overhead as this way of measuring does impose one in general
// which should at least be minimal
var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
const allocator = arena.allocator();

// probably the worst part about the entire std lib but oh well the idea and modularity is genius
// but the fact that std.hash_map.AutoContext hides the ability to choose how to handle ptrs from
// you just mitigates so many benefits
const Context = struct {
    pub const hash = std.array_hash_map
        .getAutoHashStratFn(std.builtin.SourceLocation, @This(), .Deep);
    pub const eql = std.array_hash_map
        .getAutoEqlFn(std.builtin.SourceLocation, @This());
};
var measurements = std
    .ArrayHashMap(std.builtin.SourceLocation, Measurements, Context, true).init(allocator);

pub fn record(src: std.builtin.SourceLocation, duration: us) !void {
    if (measurements.getPtr(src)) |*m| {
        m.*.sum_us += duration;
        m.*.n_measurements += 1;
    } else {
        try measurements.put(src, .{
            .src = src,
            .sum_us = duration,
            .n_measurements = 1,
        });
    }
}

pub fn results() void {
    std.debug.print(
        \\|       file       |        fn        |    took    |
        \\|------------------|------------------|------------|
        \\
    , .{});
    var iter = measurements.iterator();
    while (iter.next()) |e| {
        std.debug.print("| {s: ^16} | {s: ^16} | {d: >8}us |\n", .{
            e.key_ptr.*.file,
            e.key_ptr.*.fn_name,
            @as(usize, @intCast(e.value_ptr.sum_us)) / e.value_ptr.n_measurements,
        });
    }

    measurements.deinit();
    arena.deinit();
}
