const std = @import("std");
const vecmath = @import("vecmath.zig");
const Canvas = @import("Canvas.zig");

// weak perspective projection
pub fn draw(
    verticies: []const []const [3]f64,
    canvas: Canvas,
    cam_pos: [3]f64,
    view_dir: [3]f64,
) !void {
    const fcsize = vecmath.cast([2]f64, canvas.csize);
    const fpixels = vecmath.cast([2]f64, canvas.pixels);
    const zoom = @min(fpixels[0], fpixels[1]);
    const ratio = [2]f64{ zoom * fcsize[0] / fpixels[0], zoom * fcsize[1] / fpixels[1] };

    const xz_length = vecmath.length([2]f64{ view_dir[0], view_dir[2] });
    const pitch = std.math.atan2(f64, view_dir[1], xz_length);
    const yaw = std.math.atan2(f64, view_dir[0], view_dir[2]);
    const p = vecmath.add(cam_pos, view_dir);

    for (verticies) |vset| {
        switch (vset.len) {
            1 => {
                const v = project_to_2D(vset[0], cam_pos, p, view_dir, pitch, yaw) orelse
                    continue;
                const final = project_to_canvas(v, ratio, fcsize);
                if (final[0] < 0 or final[1] < 0)
                    continue;
                canvas.draw(vecmath.cast([2]usize, final), '.') catch
                    continue;
            },
            2 => @panic("TODO: connect two dots via a line"),
            3 => {
                const v1 = project_to_2D(vset[0], cam_pos, p, view_dir, pitch, yaw) orelse continue;
                const v2 = project_to_2D(vset[1], cam_pos, p, view_dir, pitch, yaw) orelse continue;
                const v3 = project_to_2D(vset[2], cam_pos, p, view_dir, pitch, yaw) orelse continue;
                fill_triangle(canvas, ratio, fcsize, v1, v2, v3);
            },
            else => unreachable,
        }
    }
}

// this is currently the most inefficient part of the entire program lmao
fn fill_triangle(
    canvas: Canvas,
    ratio: [2]f64,
    fcsize: [2]f64,
    v1: [2]f64,
    v2: [2]f64,
    v3: [2]f64,
) void {
    var min = project_to_canvas(.{ @min(v1[0], v2[0], v3[0]), @min(v1[1], v2[1], v3[1]) }, ratio, fcsize);
    min[0] = @max(min[0], 0);
    min[1] = @max(min[1], 0);
    var max = project_to_canvas(.{ @max(v1[0], v2[0], v3[0]), @max(v1[1], v2[1], v3[1]) }, ratio, fcsize);
    max[0] = @min(max[0], fcsize[0]);
    max[1] = @min(max[1], fcsize[1]);

    const can_v1 = project_to_canvas(v1, ratio, fcsize);
    const can_v2 = project_to_canvas(v2, ratio, fcsize);
    const can_v3 = project_to_canvas(v3, ratio, fcsize);

    var x = min[0];
    while (x < max[0]) : (x += 1) {
        var y = min[1];
        while (y < max[1]) : (y += 1) {
            const current = [2]f64{ x, y };
            if (!vecmath.in_triangle(current, can_v1, can_v2, can_v3))
                continue;
            canvas.draw(vecmath.cast([2]usize, current), '.') catch
                continue;
        }
    }
}

fn project_to_canvas(v: [2]f64, ratio: [2]f64, fcsize: [2]f64) [2]f64 {
    return .{ v[0] * ratio[0] + fcsize[0] / 2, v[1] * ratio[1] + fcsize[1] / 2 };
}

fn project_to_2D(
    vertex: [3]f64,
    cam_pos: [3]f64,
    p: [3]f64,
    view_dir: [3]f64,
    pitch: f64,
    yaw: f64,
) ?[2]f64 {
    const connector = vecmath.subtract(vertex, cam_pos);
    const t = vecmath.line_plane_intsect(cam_pos, connector, p, view_dir) orelse
        return null;
    // if this wouldnt exist one could look forward and backwards at once
    // its actually pretty fun to remove this and then move straight through the demo circle
    if (t < 0)
        return null;

    var v = vecmath.multiply(connector, t);
    v[0] = vecmath.rotate(.{ v[0], v[2] }, yaw, 0);
    v[1] = vecmath.rotate(.{ v[1], vecmath.rotate(.{ v[0], v[2] }, yaw, 1) }, pitch, 0);

    return .{ v[0], v[1] };
}
