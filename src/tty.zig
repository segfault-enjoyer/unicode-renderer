const std = @import("std");

pub const EscapeCodes = enum {
    clear,

    enter_alt_buffer,
    leave_alt_buffer,

    hide_cursor,
    show_cursor,

    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white,
    reset,

    pub fn get_code(this: EscapeCodes) []const u8 {
        return switch (this) {
            .clear => "\x1b[2J",

            .enter_alt_buffer => "\x1b[?1049h",
            .leave_alt_buffer => "\x1b[?1049l",

            .hide_cursor => "\x1b[?25l",
            .show_cursor => "\x1b[?25h",

            .black => "\x1b[30m",
            .red => "\x1b[31m",
            .green => "\x1b[32m",
            .yellow => "\x1b[33m",
            .blue => "\x1b[34m",
            .magenta => "\x1b[35m",
            .cyan => "\x1b[36m",
            .white => "\x1b[37m",
            .reset => "\x1b[0m",
        };
    }
};

var raw: bool = false;
var cooked_termios: std.os.termios = undefined;

/// Returns the typed characters as a slice.
/// Caller owns returned memory.
pub fn read(allocator: std.mem.Allocator) ![]u8 {
    return if (raw)
        try std.io.getStdIn().readToEndAlloc(allocator, std.math.maxInt(usize))
    else
        try std.io.getStdIn().reader().readUntilDelimiterOrEofAlloc(
            allocator,
            '\n',
            std.math.maxInt(usize),
        ) orelse unreachable;
}

/// Puts the terminal into raw mode
pub fn set_raw() !void {
    if (raw)
        return;
    raw = true;
    errdefer set_cooked();

    try std.io.getStdOut().writeAll(comptime EscapeCodes.get_code(.hide_cursor) ++
        EscapeCodes.get_code(.enter_alt_buffer) ++
        EscapeCodes.get_code(.clear));

    cooked_termios = try std.os.tcgetattr(std.os.STDOUT_FILENO);
    var raw_termios: std.os.termios = undefined;

    raw_termios.lflag &= ~(std.os.linux.ECHO | std.os.linux.ICANON | std.os.linux.ISIG);
    raw_termios.iflag &= ~(std.os.linux.IXON | std.os.linux.ICRNL);
    raw_termios.oflag &= ~(std.os.linux.OPOST);
    raw_termios.cflag |= std.os.linux.CS8;
    raw_termios.cc[std.os.linux.V.TIME] = 0;
    raw_termios.cc[std.os.linux.V.MIN] = 0;

    try std.os.tcsetattr(std.os.STDOUT_FILENO, .FLUSH, raw_termios);
}

/// Puts the terminal into cooked (/normal) mode
pub fn set_cooked() void {
    if (!raw)
        return;
    raw = false;

    var err: ?anyerror = null;
    defer if (err) |e|
        @panic(@errorName(e));

    std.io.getStdOut().writeAll(comptime EscapeCodes.get_code(.clear) ++
        EscapeCodes.get_code(.leave_alt_buffer) ++
        EscapeCodes.get_code(.show_cursor)) catch |e| {
        err = e;
    };

    std.os.tcsetattr(std.os.STDOUT_FILENO, .FLUSH, cooked_termios) catch |e| {
        err = e;
    };
}

pub fn get_tty_size() !std.os.linux.winsize {
    var tty_size: std.os.linux.winsize = undefined;
    return switch (std.os.linux.ioctl(
        std.os.STDOUT_FILENO,
        std.os.linux.T.IOCGWINSZ,
        @intFromPtr(&tty_size),
    )) {
        0 => tty_size,
        else => error.C_Error,
    };
}
