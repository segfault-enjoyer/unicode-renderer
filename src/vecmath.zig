//! A vector can be any indexable type, which has only one numeric type as children.
//!
//!     y+| /        v[0] = x
//!       |/   x+    v[1] = y
//! -------------    v[2] = y
//!      /|
//!   z+/ |
//!

const std = @import("std");

pub fn multiply(vec: anytype, scalar: anytype) @TypeOf(vec) {
    verify_vec(@TypeOf(vec));

    comptime std.debug.assert(is_num(@TypeOf(scalar)));
    var new_vec: @TypeOf(vec) = undefined;
    for (&new_vec, vec) |*nn, n|
        nn.* = n * scalar;
    return new_vec;
}

test "multiply" {
    try std.testing.expect(equal(multiply([3]i32{ 1, 4, -3 }, 3), .{ 3, 12, -9 }));
    try std.testing.expect(equal(multiply([3]u8{ 0, 1, 2 }, 9), .{ 0, 9, 18 }));
    try std.testing.expect(equal(multiply([3]f32{ 10, 5, -2 }, 0.5), .{ 5, 2.5, -1 }));
}

pub fn add(vec1: anytype, vec2: @TypeOf(vec1)) @TypeOf(vec1) {
    verify_vec(@TypeOf(vec1));

    var new_vec: @TypeOf(vec1) = undefined;
    for (&new_vec, vec1, vec2) |*nn, n1, n2|
        nn.* = n1 + n2;
    return new_vec;
}

test "add" {
    try std.testing.expect(equal(add([3]i32{ 1, 4, -3 }, .{ 3, 12, -9 }), .{ 4, 16, -12 }));
    try std.testing.expect(equal(add([3]u8{ 1, 4, 2 }, .{ 3, 12, 0 }), .{ 4, 16, 2 }));
    try std.testing.expect(equal(add([3]f32{ 0.2, 4, -3 }, .{ 3, 12, -9 }), .{ 3.2, 16, -12 }));
}

pub fn subtract(vec1: anytype, vec2: @TypeOf(vec1)) @TypeOf(vec1) {
    verify_vec(@TypeOf(vec1));

    var new_vec: @TypeOf(vec1) = undefined;
    for (&new_vec, vec1, vec2) |*nn, n1, n2|
        nn.* = n1 - n2;
    return new_vec;
}

test "subtract" {
    try std.testing.expect(equal(subtract([3]i32{ 1, 4, -3 }, .{ 3, 12, -9 }), .{ -2, -8, 6 }));
    try std.testing.expect(equal(subtract([3]u8{ 1, 4, 2 }, .{ 1, 1, 2 }), .{ 0, 3, 0 }));
    try std.testing.expect(equal(subtract([3]f32{ 0.2, 4, -3 }, .{ 3, 12, -9 }), .{ -2.8, -8, 6 }));
}

pub fn equal(vec1: anytype, vec2: @TypeOf(vec1)) bool {
    verify_vec(@TypeOf(vec1));

    for (vec1, vec2) |n1, n2| {
        if (n1 != n2)
            return false;
    }
    return true;
}

test "equal" {
    try std.testing.expect(equal([3]i32{ 1, 3, 2 }, .{ 1, 3, 2 }));
    try std.testing.expect(equal([3]u8{ 1, 4, 2 }, .{ 1, 4, 2 }));
    try std.testing.expect(equal([3]f32{ 0.2, 4, -3 }, .{ 0.2, 4, -3 }));
    try std.testing.expect(!equal([3]f32{ 0.1, 4, -3 }, .{ 0.2, 4, -3 }));
    try std.testing.expect(!equal([3]i32{ 0, 5, -3 }, .{ 0, 4, -4 }));
}

pub fn length(vec: anytype) @TypeOf(vec[0]) {
    verify_vec(@TypeOf(vec));

    var sum: @TypeOf(vec[0]) = 0;
    for (vec) |s|
        sum += s * s;
    return std.math.sqrt(sum);
}

test "length" {
    try std.testing.expectEqual(
        @as(u32, std.math.sqrt(@as(u32, 3 * 3 + 2 * 2 + 3 * 3))),
        length([3]u32{ 3, 2, 3 }),
    );
    try std.testing.expectEqual(
        @as(f64, std.math.sqrt(@as(f64, 3 * 3 + -2 * -2 + 0.5 * 0.5))),
        length([3]f64{ 3, -2, 0.5 }),
    );
}

pub fn scalar_product(vec1: anytype, vec2: @TypeOf(vec1)) @TypeOf(vec1[0]) {
    verify_vec(@TypeOf(vec1));

    var sum: @TypeOf(vec1[0]) = 0;
    for (vec1, vec2) |s1, s2|
        sum += s1 * s2;
    return sum;
}

pub fn angle_of_vecs(vec1: anytype, vec2: @TypeOf(vec1)) f64 {
    verify_vec(@TypeOf(vec1));

    const fvec1 = cast([3]f64, vec1);
    const fvec2 = cast([3]f64, vec2);
    return std.math.acos(
        scalar_product(fvec1, fvec2) /
            (length(fvec1) * length(fvec2)),
    );
}

test "angle_of_vecs" {
    // some direction
    const s = [3]i32{ 1, 0, 0 };
    // maximum precision of function
    const t = 1e-15;
    const pi = std.math.pi;
    try std.testing.expect(std.math.approxEqAbs(f64, 0, angle_of_vecs(s, s), t));
    try std.testing.expect(std.math.approxEqAbs(f64, pi * 0.25, angle_of_vecs(s, .{ 1, 1, 0 }), t));
    try std.testing.expect(std.math.approxEqAbs(f64, pi * 0.50, angle_of_vecs(s, .{ 0, 1, 0 }), t));
    try std.testing.expect(std.math.approxEqAbs(f64, pi * 0.75, angle_of_vecs(s, .{ -1, 1, 0 }), t));
    try std.testing.expect(std.math.approxEqAbs(f64, pi * 1.00, angle_of_vecs(s, .{ -1, 0, 0 }), t));
}

/// calculates t as the position on b+td where it intersects with (x-p)n = 0
/// returns null division by zero occured
pub fn line_plane_intsect(
    base: anytype,
    direction: @TypeOf(base),
    point_on_plane: @TypeOf(base),
    normal: @TypeOf(base),
) ?@TypeOf(base[0]) {
    verify_vec(@TypeOf(base));

    const dividend = blk: {
        var sum: @TypeOf(base[0]) = 0;
        for (normal, point_on_plane, base) |n, p, b|
            sum += n * (p - b);
        break :blk sum;
    };
    const divisor = blk: {
        var sum: @TypeOf(base[0]) = 0;
        for (normal, direction) |n, d|
            sum += n * d;
        break :blk sum;
    };

    if (@typeInfo(@TypeOf(base[0])) == .Float) {
        const tolerance = 1e-10;
        return if (-tolerance < divisor and divisor < tolerance) null else dividend / divisor;
    } else {
        return if (divisor == 0) null else @divFloor(dividend, divisor);
    }
}

test "line_plane_intsect" {
    try std.testing.expectEqual(
        @as(f64, 2.0),
        line_plane_intsect([3]f64{ 0, 0, 0 }, .{ 1, 1, 0 }, .{ 1, 2, 3 }, .{ 3, 2, 1 }).?,
    );
    try std.testing.expectEqual(
        @as(i32, 2),
        line_plane_intsect([3]i32{ 0, 0, 0 }, .{ 1, 1, 0 }, .{ 1, 2, 3 }, .{ 3, 2, 1 }).?,
    );
}

pub fn rotate(vec: anytype, angle: anytype, axis: u1) @TypeOf(vec[0]) {
    verify_vec(@TypeOf(vec));
    comptime std.debug.assert(@typeInfo(@TypeOf(angle)) == .Float or
        @typeInfo(@TypeOf(angle)) == .ComptimeFloat);
    comptime std.debug.assert(vec.len == 2);

    const fvec = cast([2]f64, vec);
    const new = switch (axis) {
        0 => fvec[0] * std.math.cos(angle) - fvec[1] * std.math.sin(angle),
        1 => fvec[0] * std.math.sin(angle) + fvec[1] * std.math.cos(angle),
    };
    return if (@typeInfo(@TypeOf(vec[0])) == .Float) @floatCast(new) else @intFromFloat(new);
}

test "rotate" {
    try std.testing.expectEqual(@as(f32, 2), rotate([2]f32{ 1, -2 }, std.math.pi / 2.0, 0));
    try std.testing.expectEqual(@as(f32, 1), rotate([2]f32{ 1, -2 }, std.math.pi / 2.0, 1));
}

pub fn area_of_triangle(v1: anytype, v2: @TypeOf(v1), v3: @TypeOf(v1)) @TypeOf(v1[0]) {
    verify_vec(@TypeOf(v1));
    comptime std.debug.assert(v1.len == 2);

    return @abs(v1[0] * (v2[1] - v3[1]) +
        v2[0] * (v3[1] - v1[1]) +
        v3[0] * (v1[1] - v2[1])) / 2.0;
}

test "area of triangle" {
    try std.testing.expectEqual(@as(f64, 2), area_of_triangle(
        [2]f64{ 0, 0 },
        [2]f64{ 0, 2 },
        [2]f64{ 2, 0 },
    ));
    try std.testing.expectEqual(@as(f64, 2), area_of_triangle(
        [2]f64{ -1, 0 },
        [2]f64{ 1, 0 },
        [2]f64{ 0, 2 },
    ));
}

pub fn in_triangle(p: anytype, v1: @TypeOf(p), v2: @TypeOf(p), v3: @TypeOf(p)) bool {
    verify_vec(@TypeOf(p));
    comptime std.debug.assert(p.len == 2);

    const area =
        area_of_triangle(p, v2, v3) +
        area_of_triangle(v1, p, v3) +
        area_of_triangle(v1, v2, p);
    return std.math.approxEqAbs(@TypeOf(p[0]), area_of_triangle(v1, v2, v3), area, 1e-10);
}

test "is in triangle" {
    try std.testing.expect(in_triangle(
        [2]f64{ 0, 1 },
        [2]f64{ -1, 0 },
        [2]f64{ 1, 0 },
        [2]f64{ 0, 2 },
    ));
    try std.testing.expect(!in_triangle(
        [2]f64{ 3, 1 },
        [2]f64{ -1, 0 },
        [2]f64{ 1, 0 },
        [2]f64{ 0, 2 },
    ));
}

pub fn cast(comptime T: type, vec: anytype) T {
    verify_vec(T);
    verify_vec(@TypeOf(vec));

    var ret_vec: T = undefined;
    inline for (&ret_vec, vec) |*rv, v| {
        switch (@typeInfo(@typeInfo(T).Array.child)) {
            .Float => switch (@typeInfo(@TypeOf(vec[0]))) {
                .Float => rv.* = @floatCast(v),
                .Int => rv.* = @floatFromInt(v),
                else => unreachable,
            },
            .Int => switch (@typeInfo(@TypeOf(vec[0]))) {
                .Float => rv.* = @intFromFloat(v),
                .Int => rv.* = @intCast(v),
                else => unreachable,
            },
            else => unreachable,
        }
    }
    return ret_vec;
}

const vector_specification =
    \\A vector can be any indexable type, which has only one numeric type as children.
    \\
;

pub fn verify_vec(comptime T: type) void {
    comptime {
        const info = @typeInfo(T);

        const valid_array = info == .Array and is_num(info.Array.child);
        const valid_vector = info == .Vector and is_num(info.Vector.child);

        const valid_tuple = blk: {
            if (info != .Struct or !info.Struct.is_tuple)
                break :blk false;

            const numT = info.Struct.fields[0].type;
            if (!is_num(numT))
                break :blk false;

            for (info.Struct.fields) |f| {
                if (f.type != numT)
                    break :blk false;
            }
            break :blk true;
        };

        if (!valid_array and !valid_vector and !valid_tuple) @compileError(std.fmt.comptimePrint(
            vector_specification ++ "This one is of type {}",
            .{T},
        ));
    }
}

test "verify_vec" {
    // shouldnt error
    verify_vec([1]u7);
    verify_vec([3]f32);
    verify_vec([9]isize);
    verify_vec(@TypeOf(.{ 0.75, 0.5 }));
    verify_vec(@TypeOf(.{ 2, -5 }));
    verify_vec(@Vector(3, f64));

    // should error
    // verify_vec(f32);
    // verify_vec([9]bool);
    // verify_vec(struct {});
    // verify_vec(@TypeOf(.{ 1, 0.5 }));
    // verify_vec(@TypeOf(.{ 2, false }));
}

fn is_num(comptime T: type) bool {
    return switch (@typeInfo(T)) {
        .Int, .Float, .ComptimeFloat, .ComptimeInt => true,
        else => false,
    };
}
